#!/usr/bin/lua

function log(msg)
    local file = io.open("/tmp/test.log", "a")
    file:write(msg.."\n")
    file:close()
end

function exec(cmd)
    local handle = io.popen(cmd)
    local result = handle:read("*a")
    handle:close()
    return result
end

function get_xinit_state()
    local file = io.open("/tmp/system.state", "r")
    if not file then
        return ""
    end
    local state = file:read()
    file:close()
    return state
end

function get_pid()
    return tonumber(exec("ps -o ppid= -p $$"))
end

function get_process(pid)
    local _, _, ppid, cmd = exec("ps -o 'ppid= cmd=' -p "..pid):find("%s*(%d+) (.*)\n")
    return {pid=pid, ppid=tonumber(ppid), cmd=cmd}
end

function get_process_tree()
    local pid = get_pid()
    while pid ~= 0 do
        local process = get_process(pid)
        log(string.format("pid=%d ppid=%d cmd='%s'", process.pid, process.ppid, process.cmd))
        pid = process.ppid
    end
end

function get_config_name()
    local xrandr_name = exec("xrandr_mgr --name")
    local home = os.getenv("HOME")
    return string.format("%s/.config/bspwm/bsp_%s.conf", home, xrandr_name)
end

function get_displays()
    local xdr = exec("xrandr")
    local displays = {}
    for name, prop in xdr:gmatch("(%S+) connected (%S+) ") do
        local is_primary = prop == "primary"
        table.insert(displays, {name=name, primary=is_primary})
    end
    return displays
end

function make_config(name, displays)
    local result = {}
    local file = io.open(name, "w")
    local ws = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
    local wsc_def = #ws//#displays
    local wsc_prm = 0
    if #displays == 1 then
        wsc_prm = #ws
    else
        wsc_prm = #ws-wsc_def*(#displays-1)
    end
    for n,d in pairs(displays) do
        local list = {}
        if d.primary then
            for i = 1,wsc_prm do
                table.insert(list, ws[i])
            end
        else
            for i = 1,wsc_def do
                local w = ws[#ws]
                table.remove(ws)
                table.insert(list, 1, w)
            end
        end
        file:write(string.format("%s ws %s\n", d.name, table.concat(list, ",")))
        result[d.name] = {ws=list, swap=nil}
    end
    file:close()
    return result
end

function split(str, sep)
    if sep == nil then
        sep = "%s"
    end
    local result = {}
    for s in str:gmatch("([^"..sep.."]+)") do
        table.insert(result, s)
    end
    return result
end

function get_config(name)
    local displays = get_displays()
    local file = io.open(name, "r")
    if not file then
        return make_config(name, displays)
    end
    local result = {}
    for n,d in pairs(displays) do
        result[d.name] = { ws = {}, swap=nil }
    end
    for name,param,value in file:read("*a"):gmatch("(%S+)%s+(%S+)%s+(%S*)\n") do
        if param == "ws" then
            result[name].ws = split(value, ",")
        elseif param == "swap" then
            result[name].swap = value
        end
    end
    file:close()
    return result
end

function get_bsp_monitors()
    local mts = exec("bspc query --monitors --names")
    return split(mts)
end

function make_fake_desktop(mts)
    for _,name in pairs(mts) do
        exec(string.format("bspc monitor %s --add-desktops %s", name, "desk_"..name))
    end
end

function make_bsp_reset(cfg)
    for name,conf in pairs(cfg) do
        exec(string.format("bspc monitor %s -d %s", name, table.concat(conf.ws, " ")))
        if conf.swap ~= nil then
            exec(string.format("bspc monitor %s -s %s", name, conf.swap))
        end
    end
end

function make_bsp_order(cfg)
    for name,conf in pairs(cfg) do
        exec(string.format("bspc monitor %s -o %s", name, table.concat(conf.ws, " ")))
    end
end

function make_bsp_move(cfg)
    for name,conf in pairs(cfg) do
        for n,desktop in pairs(conf.ws) do
            exec(string.format("bspc desktop %s -m %s", desktop, name))
        end
    end
end

function clear_desktops(monitor, ws)
    local desktops = exec(string.format("bspc query --desktops --monitor %s --names", monitor))
    for name in desktops:gmatch("(%S+)") do
        local remove = true
        for _,w in pairs(ws) do
            if tostring(w) == tostring(name) then
                remove = false
                break
            end
        end
        if remove then
            exec(string.format("bspc desktop %s --remove", name))
        end
    end
end

function clear_bsp_desktops(cfg)
    for name,conf in pairs(cfg) do
        clear_desktops(name, conf.ws)
    end
end

function clear_bsp_monitors(mts, cfg)
    for _,mts_name in pairs(mts) do
        local remove = true
        for cfg_name,_ in pairs(cfg) do
           if cfg_name == mts_name then
               remove = false
               break
           end
        end
        if remove then
            exec(string.format("bspc monitor %s --remove", mts_name))
        end
    end
end

function on_login()
    local name = get_config_name()
    local cfg = get_config(name)
    make_bsp_reset(cfg)
end

function on_reload()
    local name = get_config_name()
    local cfg = get_config(name)
    local mts = get_bsp_monitors()
    make_fake_desktop(mts)
    make_bsp_move(cfg)
    make_bsp_order(cfg)
    clear_bsp_desktops(cfg)
    clear_bsp_monitors(mts, cfg)
end

if arg[1] == "0" then
    on_login()
elseif arg[1] == "3" then
    on_reload()
end

