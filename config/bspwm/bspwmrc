#!/bin/bash

# xrandr monitors
xrandr_mgr --run

# numlockx
killall -q numlockx
numlockx &

# feh
feh --bg-fill ~/Pictures/bg.jpg

# picom
picom &

# conky
killall -q conky
conky &
conky --config ~/.config/conky/network.conf &
conky --config ~/.config/conky/cpu.conf &
conky --config ~/.config/conky/memory.conf &

# workspaces
~/.config/bspwm/monitor.lua $1

# polybar
killall -q polybar
while pgrep -u $UID -x polybar > /dev/null; do sleep 1; done
for m in $(xrandr --query | grep " connected" | cut -d ' ' -f1); do
    MONITOR=$m polybar --reload topbar &
done

# only when start X
if [ $1 == "0" ]; then
    # power manager
    xset dpms 420
    xidlehook --not-when-fullscreen --not-when-audio \
        --timer 270 'notify-send "The screen is locked for 30 seconds!"' '' \
        --timer 300 'locker' '' \
        --timer 600 'systemctl suspend' '' &
    xss-lock -- locker &
    # xkb
    xkbcomp ~/.config/xkb/config $DISPLAY
    xcape -t 1000 -e "Control_L=Escape"
    # sxhkd
    killall -q sxhkd
    sxhkd &
    # perwindowlayout
    perWindowLayoutD
fi

# border and gaps
bspc config border_width 1
bspc config window_gap 10
bspc config normal_border_color "#23252e"
bspc config active_border_color "#23252e"
bspc config focused_border_color "#928374"

# parent split
bspc config split_ratio 0.5
bspc config focus_follows_pointer true
bspc config pointer_modifier super

# monocle if open one window
bspc config single_monocle false
bspc config borderless_monocle false
bspc config gapless_monocle false
bspc config paddingless_monocle true

# scheme of open new window
#bspc config automatic_scheme spiral

# window rules
#bspc rule -a Firefox state=floating rectangle=1000x700+10+20 desktop='^1'
