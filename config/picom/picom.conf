backend = "glx";
experimental-backends = true;
glx-no-stencil = true;
glx-copy-from-front = false;

# Opacity
active-opacity = 1;
inactive-opacity = 1;
frame-opacity = 1;
inactive-opacity-override = false;
blur-background = true;
blur-background-exclude = [
#	"window_type = 'dock'",
	"class_g = 'conky'",
    "class_g = 'TelegramDesktop'"
];

blur-method = "dual_kawase";
blur-strength = 6;

# Fading
fading = true;
fade-delta = 4;
no-fading-openclose = false;
fade-exclude = [ ];

# Other
mark-wmwin-focused = true;
mark-ovredir-focused = true;
detect-rounded-corners = true;
detect-client-opacity = true;
refresh-rate = 0;
vsync = true;
dbe = false;
unredir-if-possible = false;
focus-exclude = [ ];
detect-transient = true;
detect-client-leader = true;

# Window type settings
wintypes:
{
	dock = {
		shadow = false;
	};
};

opacity-rule = [
	"100:class_g = 'URxvt'",
	"100:class_g = 'Alacritty' && focused",
	"95:class_g = 'Alacritty' && !focused"
];

shadow = true;
shadow-radius = 15;
shadow-offset-x = -14;
shadow-offset-y = -14;
shadow-opacity = 1.0;
#shadow-green = 1.0;

shadow-exclude = [
	"! name~=''",
	"name = 'Docky'",
	"name = 'Kupfer'",
	"name = 'cpt_frame_window'",
	"name *= 'VLC'",
	"name *= 'compton'",
	"name *= 'picom'",
	"name *= 'Chromium'",
	"name *= 'Chrome'",
	"class_g = 'Firefox' && argb",
	"class_g = 'Conky'",
	"class_g = 'Kupfer'",
	"class_g = 'Synapse'",
	"class_g ?= 'Notify-osd'",
	"class_g ?= 'Cairo-dock'",
	"class_g ?= 'Xfce4-notifyd'",
	"class_g ?= 'Xfce4-power-manager'",
	"_GTK_FRAME_EXTENTS@:c",
	"_NET_WM_STATE@:32a *= '_NET_WM_STATE_HIDDEN'"
];

corner-radius = 10;
round-borders = 1;
xrender-sync-fence = true;

rounded-corners-exclude = [
    "class_g = 'Polybar'"
];
