require 'cairo'

global = {
    history = {}
}

function log(text)
    local file = io.open('/tmp/conky.log', 'a')
    file:write(text..'\n')
    file:close()
end

function exec(cmd)
    local handle = io.popen(cmd)
    local output = handle:read("*a")
    handle:close()
    return output
end

function draw_graph(cr, dx, dy, loaded)
    table.insert(global.history, loaded)
    local size = #(global.history)
    if size > 57 then
        table.remove(global.history, 1)
        size = size-1
    end
    -- draw
    cairo_set_line_width(cr, 1)
    -- loaded
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.1)
    cairo_move_to(cr, dx+3, dy)
    for i = 1, size do
        cairo_line_to(cr, dx+i*3, dy-80*global.history[i]/100.0)
    end
    cairo_line_to(cr, dx+size*3, dy)
    cairo_close_path(cr)
    cairo_fill(cr)
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0)
    cairo_move_to(cr, dx+3, dy-80*global.history[1]/100.0)
    for i = 2, size do
        cairo_line_to(cr, dx+i*3, dy-80*global.history[i]/100.0)
    end
    cairo_stroke(cr)
end

function draw_cpu(cr, dx, dy)
    local rad = math.pi/180.0
    local scale = 180.0
    if global.nproc == nil then
        global.nproc = tonumber(exec("nproc"))
    end
    local nproc = global.nproc
    local cpu0 = tonumber(conky_parse("${cpu cpu0}"))

    local request = ""
    local regreq = ""
    for n = 0, nproc do
        request = request..string.format(" ${cpu cpu%d}", n)
        regreq = regreq.."%s(%d+)"
    end
    local cpu = {string.find(conky_parse(request), regreq)}
    -- draw general
    for n = 0, nproc do
        local loaded = cpu[3+n]
        local radius = 97 - n*6
        if n == 0 then
            cairo_set_line_width(cr, 10.0)
            radius = radius + 3
        else
            cairo_set_line_width(cr, 4.0)
            radius = radius - (n%2)
        end
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.2)
        cairo_arc(cr, dx+150, dy+110, radius, 270.0*rad, (270.0+scale)*rad)
        cairo_stroke(cr)
        cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0)
        cairo_arc(cr, dx+150, dy+110, radius, 270.0*rad, (270.0+loaded/100.0*scale)*rad)
        cairo_stroke(cr)
    end
    -- draw graph
    draw_graph(cr, dx-3, 150+dy, tonumber(cpu[3]))
    -- text
    local text = string.format('Average CPU usage %3d%%', cpu[3])
    cairo_move_to(cr, dx+8, dy+15)
    cairo_show_text(cr, text)
    -- text every cpu
    for n=1, nproc/2 do
        local first = n*2-1
        local second = n*2
        local text = string.format("CPU %d %3d%% | CPU %d %3d%%", first, cpu[3+first], second, cpu[3+second])
        cairo_move_to(cr, dx+3, dy+15+n*12)
        cairo_show_text(cr, text)
    end

end

function conky_main()
    if conky_window == nil then return end
    local width = conky_window.width
    local height = conky_window.height
    local cs=cairo_xlib_surface_create(conky_window.
                                       display,
                                       conky_window.drawable,
                                       conky_window.visual,
                                       width,
                                       height)
    cr=cairo_create(cs)
    
    cairo_select_font_face(cr, "JetBrains Mono Nerd Font", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL)
    cairo_set_font_size(cr, 10)
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND)
    draw_cpu(cr, 20, 30)

    cairo_destroy(cr)
    cairo_surface_destroy(cs)
    cr=nil
end

