require 'cairo'

global = {
    iface = {
        time = os.time(),
        delay = 10,
    },
    network = {}
}

function log(text)
    local file = io.open('/tmp/conky.log', 'a')
    file:write(text..'\n')
    file:close()
end

function exec(cmd)
    local handle = io.popen(cmd)
    local output = handle:read("*a")
    handle:close()
    return output
end

function get_interfaces()
    if global['iface']['result'] == nil or global['iface']['time'] + global['iface']['delay'] < os.time() then
        global['iface']['time'] = os.time()
        local iface = exec("ip -o address")
        local obj={}
        for name,type,address in iface:gmatch("%d+: (%w+)%s+(inet6?)%s+(%S+)/%d+") do
            if name ~= 'lo' then
                if obj[name] == nil then obj[name] = {} end
                obj[name][type] = address
                obj[name]["name"] = name
            end
        end
        -- result
        global['iface']['result'] = obj
        return obj
    else
        return global['iface']['result']
    end
end

function get_traffic(iface)
    local _, _, downspeedf, downspeed, totaldown, upspeedf, upspeed, totalup = string.find(conky_parse("${downspeedf "..iface.."} ${downspeed "..iface.."} ${totaldown "..iface.."} ${upspeedf "..iface.."} ${upspeed "..iface.."} ${totalup "..iface.."}"), "(%S+) (%S+) (%S+) (%S+) (%S+) (%S+)")
    return tonumber(downspeedf), downspeed, totaldown, tonumber(upspeedf), upspeed, totalup
end

function draw_graph(cr, dx, dy, name, speed_down, max_down, speed_up, max_up)
    table.insert(global.network[name].down.history, speed_down)
    table.insert(global.network[name].up.history, speed_up)
    local size = #(global.network[name].down.history)
    if size > 58 then
        table.remove(global.network[name].down.history, 1)
        table.remove(global.network[name].up.history, 1)
        size = size-1
    end
    -- draw
    cairo_set_line_width(cr, 1)
    -- down
    local history = global.network[name].down.history
    cairo_move_to(cr, dx+3, 60+dy-1-20*history[1]/max_down)
    for i = 2, size do
        cairo_line_to(cr, dx+i*3, 60+dy-1-20*history[i]/max_down)
    end
    cairo_stroke(cr)
    -- up
    local history = global.network[name].up.history
    cairo_move_to(cr, dx+3, 60+dy+1+20*history[1]/max_up)
    for i = 2, size do
        cairo_line_to(cr, dx+i*3, 60+dy+1+20*history[i]/max_up)
    end
    cairo_stroke(cr)
    -- text
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.5)
    -- down text
    cairo_move_to(cr, dx, 60+dy-15)
    cairo_show_text(cr, global.network[name].down.text_max)
    -- up text
    cairo_move_to(cr, dx, 60+dy+25)
    cairo_show_text(cr, global.network[name].up.text_max)
end

function draw_network(cr, network, dx, dy)
    local name = network.name
    -- update traffic
    local rxf, rx, trx, txf, tx, ttx = get_traffic(name)
    if global.network[name] == nil then
        global.network[name] = {
            name = name,
            down = {
                speed_max = math.max(rxf, 0.1),
                text_max = rx,
                total = trx,
                history = {}
            },
            up = {
                speed_max = math.max(txf, 0.1),
                text_max = tx,
                total = ttx,
                history = {}
            }
        }
    else
        local down_max = global.network[name].down.speed_max;
        if down_max < rxf then
            global.network[name].down.speed_max = rxf
            global.network[name].down.text_max = rx
        end
        global.network[name].down.total = trx
        local up_max = global.network[name].up.speed_max;
        if up_max < txf then
            global.network[name].up.speed_max = txf
            global.network[name].up.text_max = tx
        end
        global.network[name].down.total = ttx
    end
    -- set variables
    local rad = math.pi/180.0
    local scale = 180.0
    local max_down = global.network[name].down.speed_max;
    local max_down_text = global.network[name].down.text_max;
    local max_up = global.network[name].up.speed_max;
    local max_up_text = global.network[name].up.text_max;
    -- draw down
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.2)
    cairo_set_line_width(cr, 10.0)
    cairo_arc(cr, dx+150.0, 60.0+dy, 50.0, 270.0*rad, (270.0+scale)*rad )
    cairo_stroke(cr)
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0)
    cairo_arc(cr, dx+150.0, 60.0+dy, 50.0, 270.0*rad, (270.0+rxf/max_down*scale)*rad )
    cairo_stroke(cr)
    -- draw up
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.2)
    cairo_set_line_width(cr, 5.0)
    cairo_arc(cr, dx+150.0, 60.0+dy, 40.0, 270.0*rad, (270.0+scale)*rad )
    cairo_stroke(cr)
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0)
    cairo_arc(cr, dx+150.0, 60.0+dy, 40.0, 270.0*rad, (270.0+txf/max_up*scale)*rad )
    cairo_stroke(cr)
    -- up text
    local text = string.format('Up:   %7s / %7s', ttx, tx)
    cairo_move_to(cr, dx, dy+25)
    cairo_show_text(cr, text)
    -- down text
    local text = string.format('Down: %7s / %7s', trx, rx)
    cairo_move_to(cr, dx, dy+12)
    cairo_show_text(cr, text)
    --
    local text = string.format('iface: %s', name)
    cairo_move_to(cr, dx, dy+110)
    cairo_show_text(cr, text)
    -- draw graph
    draw_graph(cr, dx, dy+5, name, rxf, max_down, txf, max_up)
    -- delete cursor
    cairo_stroke(cr)
end

function conky_main()
    if conky_window == nil then return end
    local width = conky_window.width
    local height = conky_window.height
    local cs=cairo_xlib_surface_create(conky_window.
                                       display,
                                       conky_window.drawable,
                                       conky_window.visual,
                                       width,
                                       height)
    cr=cairo_create(cs)
    local updates=tonumber(conky_parse('${updates}'))
    
    local obj = get_interfaces()

    --cairo_rectangle(cr, 0, 0, 200, 200)
    cairo_select_font_face(cr, "JetBrains Mono Nerd Font", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL)
    cairo_set_font_size(cr, 10)
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND)
    local dy = 50
    for iface,network in pairs(obj) do
        obj[iface].traffic = get_traffic(iface)
        draw_network(cr, network, 30, dy)
        dy = dy + 150.0
    end
    --cairo_paint_with_alpha(cr, 0.5)

    cairo_destroy(cr)
    cairo_surface_destroy(cs)
    cr=nil
end

