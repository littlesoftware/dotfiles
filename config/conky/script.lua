require 'cairo'

function exec(cmd)
    local handle = io.popen(cmd)
    local result = handle:read("*a")
    handle:close()
    return result
end

function log(text)
    local file = io.open("/tmp/conky.log", "a")
    file:write(text.."\n")
    file:close()
end

function conky_main()
    if conky_window == nil then return end
    local width = conky_window.width
    local height = conky_window.height
    local cs=cairo_xlib_surface_create(conky_window.display,
                                         conky_window.drawable,
                                         conky_window.visual,
                                         width,
                                         height)
    cr=cairo_create(cs)
    local updates=tonumber(conky_parse('${updates}'))

    --log (string.format("updates = %d %dx%d", updates, width, height))
    --if updates>5 then
    --end

    --local result = os.getenv("HOME").."/.config/conky/"; -- exec("pwd")
    --log (string.format("ls: '%s'", result))

    --cairo_set_source_rgb(cr, 0.0, 0.0, 0.0)
    --cairo_paint_with_alpha(cr, 0.5)

    cairo_destroy(cr)
    cairo_surface_destroy(cs)
    cr=nil
end

