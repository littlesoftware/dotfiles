require 'cairo'

global = {
    mem = {},
    swap = {}
}

function log(text)
    local file = io.open('/tmp/conky.log', 'a')
    file:write(text..'\n')
    file:close()
end

function exec(cmd)
    local handle = io.popen(cmd)
    local output = handle:read("*a")
    handle:close()
    return output
end

function get_mem_info()
    local _, _, mem, memmax, memperc, swap, swapmax, swapperc = string.find(conky_parse("${mem} ${memmax} ${memperc} ${swap} ${swapmax} ${swapperc}"), "(%S+) (%S+) (%S+) (%S+) (%S+) (%S+)")
    return mem, memmax, tonumber(memperc), swap, swapmax, tonumber(swapperc)
end

function draw_graph(cr, dx, dy, mem, swap)
    table.insert(global.mem, mem)
    table.insert(global.swap, swap)
    local size = #(global.mem)
    if size > 60 then
        table.remove(global.mem, 1)
        table.remove(global.swap, 1)
        size = size-1
    end
    -- draw
    -- mem
    cairo_set_line_width(cr, 1)
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.1)
    cairo_move_to(cr, dx+3, dy)
    for i = 1, size do
        cairo_line_to(cr, dx+i*3, dy-45*global.mem[i]/100.0)
    end
    cairo_line_to(cr, dx+size*3, dy)
    cairo_close_path(cr)
    cairo_fill(cr)
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0)
    cairo_move_to(cr, dx+3, dy-45*global.mem[1]/100.0)
    for i = 2, size do
        cairo_line_to(cr, dx+i*3, dy-45*global.mem[i]/100.0)
    end
    cairo_stroke(cr)
    -- swap
    cairo_set_source_rgba(cr, 0.5, 0.0, 0.0, 0.1)
    cairo_move_to(cr, dx+3, dy)
    for i = 1, size do
        cairo_line_to(cr, dx+i*3, dy-45*global.swap[i]/100.0)
    end
    cairo_line_to(cr, dx+size*3, dy)
    cairo_close_path(cr)
    cairo_fill(cr)
    cairo_set_source_rgba(cr, 0.5, 0.0, 0.0, 1.0)
    cairo_move_to(cr, dx+3, dy-45*global.swap[1]/100.0)
    for i = 2, size do
        cairo_line_to(cr, dx+i*3, dy-45*global.swap[i]/100.0)
    end
    cairo_stroke(cr)
end

function draw_memory(cr, dx, dy)
    local rad = math.pi/180.0
    local scale = 225.0
    local mem, memmax, memperc, swap, swapmax, swapperc = get_mem_info()
    -- draw general
    -- mem
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.2)
    cairo_set_line_width(cr, 10.0)
    cairo_arc_negative(cr, dx+60, dy+60, 50.0, 270.0*rad, (270.0-scale)*rad)
    cairo_stroke(cr)
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0)
    cairo_arc_negative(cr, dx+60, dy+60, 50.0, 270.0*rad, (270.0-memperc*scale/100.0)*rad)
    cairo_stroke(cr)
    -- swap
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.2)
    cairo_set_line_width(cr, 5.0)
    cairo_arc_negative(cr, dx+60, dy+60, 40.0, 270.0*rad, (270.0-scale)*rad)
    cairo_stroke(cr)
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0)
    cairo_arc_negative(cr, dx+60, dy+60, 40.0, 270.0*rad, (270.0-swapperc*scale/100.0)*rad)
    cairo_stroke(cr)
    -- draw graph
    draw_graph(cr, 30+dx, 83+dy, memperc, swapperc)
    -- text
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0)
    local text = string.format('RAM:  %7s / %7s', mem, memmax)
    cairo_move_to(cr, dx+70, dy+14)
    cairo_show_text(cr, text)
    local text = string.format('Swap: %7s / %7s', swap, swapmax)
    cairo_move_to(cr, dx+70, dy+27)
    cairo_show_text(cr, text)

end

function conky_main()
    if conky_window == nil then return end
    local width = conky_window.width
    local height = conky_window.height
    local cs=cairo_xlib_surface_create(conky_window.
                                       display,
                                       conky_window.drawable,
                                       conky_window.visual,
                                       width,
                                       height)
    cr=cairo_create(cs)
    
    cairo_select_font_face(cr, "JetBrains Mono Nerd Font", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL)
    cairo_set_font_size(cr, 10)
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND)
    draw_memory(cr, 0, 35)

    cairo_destroy(cr)
    cairo_surface_destroy(cs)
    cr=nil
end

