call plug#begin()

" Colorschemes
Plug 'morhetz/gruvbox'
" Airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" NerdTree
Plug 'preservim/nerdtree'
" Auto Pairs
Plug 'jiangmiao/auto-pairs'
" Git
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
" Ctrlp
Plug 'ctrlpvim/ctrlp.vim'
" Surround
Plug 'tpope/vim-surround'
" Sneak
Plug 'justinmk/vim-sneak'
" Coc
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'neoclide/coc.nvim'
" Colors
Plug 'lilydjwg/colorizer'
" Icons
Plug 'ryanoasis/vim-devicons'
" Formating code
Plug 'rhysd/vim-clang-format'
" Switch C/H
Plug 'ericcurtin/CurtineIncSw.vim'
" Fzf
Plug 'junegunn/fzf', { 'do': { -> fzf#install()  }  }
Plug 'junegunn/fzf.vim'

call plug#end()

" Settings tab
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set softtabstop=4
set autoindent
set t_Co=256
set number relativenumber
set scrolloff=10

syntax on

set mousehide
set mouse=a
set termencoding=utf-8
set novisualbell
set t_vb=

set showtabline=1
set wrap
set linebreak

set nobackup
set noswapfile
set encoding=utf-8
set fileencodings=utf-8,cp1251

set clipboard=unnamed
set ruler

set hidden

set visualbell t_vb=
autocmd FileType * setlocal formatoptions-=o

" Debug
let g:termdebug_wide=1

" Theme
let g:airline_powerline_fonts = 1
let g:airline_left_sep = ''
let g:airline_right_sep = ''
set background=dark
let g:gruvbox_contrast_dark="hard"
let g:gruvbox_transparent_bg=1
let g:gruvbox_invert_selection=0
set cursorline
colorscheme gruvbox
function s:updateColors()
    hi Normal ctermbg=none
    hi! link CocErrorHighlight CocUnderline
endf
autocmd VimEnter * call s:updateColors()

call clang_format#start_format_on_insert()
inoremap <esc> <esc>:call clang_format#format_area()<cr>

let g:clang_format#style_options = {
    \ "BasedOnStyle" : "Google",
    \ "SpacesInSquareBrackets" : "true",
    \ "SpacesInParentheses" : "true",
    \ "SpacesInAngles" : "true",
    \ "SpaceBeforeParens" : "Never",
    \ "IndentWidth" : 2,
    \ "IndentCaseLabels" : "true",
    \ "FixNamespaceComments" : "true",
    \ "BreakConstructorInitializers" : "BeforeComma",
    \ "AllowShortFunctionsOnASingleLine" : "None",
    \ "BreakBeforeBraces" : "Custom",
    \ "BraceWrapping" : {
        \ "AfterClass" : "true",
        \ "AfterControlStatement" : "true",
        \ "AfterEnum" : "true",
        \ "AfterFunction" : "true",
        \ "AfterNamespace" : "true",
        \ "AfterStruct" : "true",
        \ "AfterUnion" : "true",
        \ "AfterExternBlock" : "true",
        \ "BeforeCatch" : "true",
        \ "BeforeElse" : "true",
        \ "SplitEmptyFunction" : "false"
    \ }
\ }

" autocmd FileType c,cpp,objc ClangFormatAutoEnable
"let g:clang_format#auto_format_on_insert_leave = 1
"let g:clang_format#auto_format = 1
let mapleader = "\<space>"

" search
set hlsearch
let g:highlighting = 0
function! Highlighting()
    if g:highlighting == 1 && @/ =~ expand('<cword>')
        let g:highlighting = 0
        return ":silent nohlsearch\<cr>"
    endif
    let @/ = expand('<cword>')
    let g:highlighting = 1
    return ":silent set hlsearch\<cr>"
endfunction
nnoremap <silent> <expr> <leader>8 Highlighting()
nnoremap <silent> <leader>/ :noh<cr>:let g:highlighting = 0<cr>

map f <Plug>Sneak_f
map F <Plug>Sneak_F
map t <Plug>Sneak_t
map T <Plug>Sneak_T

nnoremap <leader>fb :Buffers<cr>
nnoremap <leader>ff :Files<cr>
nnoremap <leader>ft :Tags<cr>
nnoremap <leader>fg :Rg<cr>


" Mapping

nnoremap <leader>m :call MyMake()<cr>
nnoremap <leader>dd :call MyDebug()<cr>
nnoremap <leader>rr :!./some<cr>
nnoremap <leader>dr :Run<cr>
nnoremap <leader>db :Break<cr>
nnoremap <leader>dc :Clear<cr>
nnoremap <leader>ds :Step<cr>
nnoremap <leader>dn :Over<cr>
nnoremap <leader>df :Finish<cr>
nnoremap <leader>de :Continue<cr>
nnoremap <leader>dk :Stop<cr>

autocmd FileType c,cpp,objc vnoremap <buffer><leader>cf :ClangFormat<cr>

nnoremap <leader>fs :call CurtineIncSw()<cr>
nnoremap <silent> <leader>re <Plug>(coc-codeaction-refactor)
nnoremap <silent> <leader>rs <Plug>(coc-codeaction-refactor-selected)
vnoremap <silent> <leader>re <Plug>(coc-codeaction-refactor-selected)
nnoremap <silent> <leader>ra <Plug>(coc-codeaction-cursor)

"nnoremap <silent> gs :call CocAction('jumpDefinition', 'split')<cr>
"nnoremap <silent> gd :call CocAction('jumpDefinition', 'vsplit')<cr>
"nnoremap <silent> gt :call CocAction('jumpDefinition', 'tabe')<cr>
nnoremap <silent> gd <Plug>(coc-definition)
nnoremap <silent> gy <Plug>(coc-type-definition)
nnoremap <silent> gi <Plug>(coc-implementation)
nnoremap <silent> gr <Plug>(coc-references)
nnoremap <silent> rn <Plug>(coc-rename)

nnoremap <leader>ce :vsplit $MYVIMRC<cr>
nnoremap <leader>cs :source $MYVIMRC<cr>
nnoremap <silent> [e <Plug>(coc-diagnostic-prev)
nnoremap <silent> ]e <Plug>(coc-diagnostic-next)
noremap <up> <nop>
noremap <down> <nop>
noremap <left> <nop>
noremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
inoremap <c-u> <esc>g~iwea

" inoremap jj j
" inoremap jk <esc>
" inoremap <esc> <nop>
noremap <c-n> :NERDTreeToggle<cr> 

" using tab
let g:coc_snippet_next='<tab>'
let g:coc_snippet_prev='<s-tab>'
inoremap <silent><expr> <tab>
        \ pumvisible() ? "\<c-n>" : "\<tab>"
inoremap <silent><expr> <s-tab>
        \ pumvisible() ? "\<c-p>" : coc#refresh()
inoremap <silent><expr> <cr>
        \ coc#pum#visible() ? coc#pum#confirm() :
        \ "\<cr>"
inoremap <c-tab> <c-v><tab>

